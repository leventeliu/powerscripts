﻿# Function helps to escape raw stirng for file path
Function Get-EscapedFilePath {
    Param([string]$rawString)
    $rawString.Trim().
        Replace(" : ", " - ").Replace(": ", " - ").Replace(":", "-").   # ��Foo : Bar��   =>  ��Foo - Bar��
                                                                        # "Foo: Part 1" =>  "Foo - Part 1"
                                                                        # "At 3:45"     =>  "At 3-45"
        Replace("*", "-").                                              # "A*Teens"     =>  "A-Teens"
        Replace("?", "").TrimEnd().                                     # "Do you   ?"  =>  "Do you"
        Replace("\", "@").Replace("/", "@").Replace("|", "@").          # "A/B|C\D"     =>  "A@B@C@D"
        Replace("<", "[").Replace(">", "]").                            # "<Part 1>"    =>  "[Part 1]"
        Replace("`"", "'").                                             # '"The voge"'  =>  ����The voge����
        TrimEnd(".")
}

Function Rename-FontFiles {
    <#
    .SYNOPSIS

    .DESCRIPTION
    Rename font file name as its title (usually the font face)

    .PARAMETER Root
    Root directory to check
    .PARAMETER CheckAndMove
    Check and move. Check and list only by default
    .PARAMETER $Extensions
    File extensions to be checked
    #>

    [CmdletBinding()]Param(
        [String]$Root = $PWD,
        [Switch]$CheckAndMove,
        [String[]]$Extensions = @(
            '.otf',
            '.ttc',
            '.ttf')
    )

    # Get all directories
    $alldirs = @(Get-Item -LiteralPath $Root) + (Get-ChildItem -LiteralPath $Root -Recurse -Directory)

    # Explore files
    $shellObject = New-Object -COMObject Shell.Application
    foreach ($item in $alldirs) {
        $shellView = $shellObject.Namespace($item.FullName)

        Get-ChildItem -LiteralPath $item.FullName -File | ForEach-Object {
            $shellFile = $shellView.ParseName($_.Name)
            $attrExt = $shellView.GetDetailsOf($shellFile, 159)

            if (-Not ($Extensions -Contains $attrExt.ToLower())) {
                Write-Verbose -Message "Skip a file with unknown extension: $($_.FullName)"
                return
            }

            $attrTitle = $shellView.GetDetailsOf($shellFile, 21).Trim()

            if ([string]::IsNullOrEmpty($attrTitle)) {
                Write-Warning -Message "Skip a file with unknown title: $($_.FullName)"
                return
            }

            $newFileName = Get-EscapedFilePath("{0}{1}" -f $attrTitle, $attrExt)
            $newFilePath = Join-Path $item.FullName $newFileName

            # Check file path
            if ($_.FullName -eq $newFilePath) {
                Write-Verbose -Message "File status OK: $($_.FullName)"
                return
            }

            # Check alternative file
            if (Test-Path -LiteralPath $newFilePath -PathType Leaf) {
                if (Compare-Object -ReferenceObject $(Get-Content $_.FullName) -DifferenceObject $(Get-Content $newFilePath)) {
                    Write-Host -ForegroundColor Red @"
[WARN] File is left as-is: $($_.FullName)
      alternative versoin: $newFilePath
"@
                } else {
                    Write-Host -ForegroundColor Green @"
[INFO] Remove file: $($_.FullName)
      same version: $newFilePath
"@
                    if ($CheckAndMove) {
                        Remove-Item -LiteralPath $_.FullName
                    }
                }

                return
            }

            Write-Host -ForegroundColor Green @"
[INFO] Move file: $($_.FullName)
              to: $newFilePath
"@
            if ($CheckAndMove) {
                Move-Item -LiteralPath $_.FullName -Destination $newFilePath -Force
            }
        }
    }
}

Export-ModuleMember -Function Rename-FontFiles