Write-Host -ForegroundColor Green -NoNewline "Adding repo to PSModulePath: $PSScriptRoot..."

if ($env:PSModulePath.Contains($PSScriptRoot)) {
    Write-Host -ForegroundColor Green "
==> Already exists in PSModulePath:"
    $env:PSModulePath.Split(";")
    Write-Host -ForegroundColor Green "Reloading all modules..."
    Import-Module -Verbose -Force FontDictator, MusicDictator
    Write-Host -ForegroundColor Green "Done."
} else {
    [Environment]::SetEnvironmentVariable("PSModulePath", "$env:PSModulePath;$PSScriptRoot")
    Write-Host -ForegroundColor Green "Done."
}