﻿Function Get-EscapedFilePath {
    Param([string]$rawString)
    $rawString.Trim().
    Replace(" : ", " - ").Replace(": ", " - ").Replace(":", "-").   # “Foo : Bar”   =>  “Foo - Bar”
    # "Foo: Part 1" =>  "Foo - Part 1"
    # "At 3:45"     =>  "At 3-45"
    Replace("*", "-").                                              # "A*Teens"     =>  "A-Teens"
    Replace("?", "").TrimEnd().                                     # "Do you   ?"  =>  "Do you"
    Replace("\", "@").Replace("/", "@").Replace("|", "@").          # "A/B|C\D"     =>  "A@B@C@D"
    Replace("<", "[").Replace(">", "]").                            # "<Part 1>"    =>  "[Part 1]"
    Replace("`"", "'").                                             # '"The voge"'  =>  ”‘The voge’”
    TrimEnd(".")
}

function Get-Continue {
    while ($true) {
        $resp = Read-Host -Prompt "Done scanning, continue to move files? [Y/n]"
        if ($resp -match "\w*y.*") {
            "y"
            break
        }
        if ($resp -match "\w*n.*") {
            "n"
            break
        }
    }
}

function Get-UniformArtist {
    Param(
        [string]$Artist,
        [string]$Album
    )

    if ($Album -match "银魂" -or
        $Album -match "銀魂") {
        Write-Verbose -Message "Use uniform artist `"銀魂`" for `"$Artist`""
        $result = "銀魂"
    }
    elseif ($Album -match "ポケモン" -or $Album -imatch "pokemon" -or $Album -imatch "pokémon") {
        Write-Verbose -Message "Use uniform artist `"Pokémon`" for `"$Artist`""
        $result = "Pokémon"
    }
    elseif ($Artist -match "原声" -or
        $Artist -match "原聲" -or
        $Artist -match "Soundtrack" -or
        $Artist -match "O\.S\.T" -or
        $Artist -match "O\. S\. T" -or
        $Artist -match "サウンドトラック" -or
        $Album -match "原声" -or
        $Album -match "原聲" -or
        $Album -match "Soundtrack" -or
        $Album -match "O\.S\.T" -or
        $Album -match "O\. S\. T" -or
        $Album -match "サウンドトラック") {
        Write-Verbose -Message "Use uniform artist `"Soundtrack`" for `"$Artist`"/`"$Album`""
        $result = "Soundtrack"
    }
    elseif ($Artist -match "群星" -or $Artist -eq "V\.A\.") {
        Write-Verbose -Message "Use uniform artist `"Various Artists`" for `"$Artist`""
        $result = "Various Artists"
    }
    else {
        $result = Get-EscapedFilePath($Artist)
    }

    $result
}

function ConvertFrom-HumanReadablePrefix {
    param (
        [string]$Prefix
    )

    switch ($Prefix) {
        '' { [int64]1; break }
        'k' { [int64]1 -shl 10; break }
        'm' { [int64]1 -shl 20; break }
        'g' { [int64]1 -shl 30; break }
        't' { [int64]1 -shl 40; break }
        default { throw "Unexpected human readable prefix: {0}" -f $Prefix }
    }
}

function ConvertFrom-HumanReadableBitrate {
    param (
        [string]$Bitrate
    )

    $lw = $Bitrate.ToLower()
    $mc = [regex]::Match($lw, "(\d+)\s*(\w{0,1})bps")

    if (-Not $mc.Success -or -not $mc.Groups[1].Success -or -not $mc.Groups[2].Success) {
        throw "Unexpected human readable bitrate value: {0}" -f $Bitrate
    }

    [int64]$mc.Groups[1].Value * $(ConvertFrom-HumanReadablePrefix($mc.Groups[2].Value))
}

function Compare-MusicFileObject {
    param (
        [psobject]$X,
        [psobject]$Y
    )

    $xb = ConvertFrom-HumanReadableBitrate($X.'bitrate')
    $yb = ConvertFrom-HumanReadableBitrate($Y.'bitrate')
    Write-Debug -Message "Comparing bitrate: $($X.'origin') with $($X.'bitrate')($xb) <=> $($Y.'origin') with $($Y.'bitrate')($yb)"

    if ($xb -gt $yb) {
        $true
        return
    }

    if ($xb -lt $yb) {
        $false
        return
    }

    $xs = $(Get-Item -LiteralPath $X.'origin').Length
    $ys = $(Get-Item -LiteralPath $Y.'origin').Length
    Write-Debug -Message "Comparing file size: $($X.'origin') with $($X.'size')($xs) <=> $($Y.'origin') with $($Y.'size')($ys)"

    if ($xs -gt $ys) {
        $true
        return
    }

    if ($xs -lt $ys) {
        $false
        return
    }

    $false
}

Function Rename-MusicFiles {
    <#
    .SYNOPSIS

    Scan and rearrange music files.

    .DESCRIPTION

    The preset path config is:

        <RootPath>\<AlbumArtist>\<Album>\[disc <disc#\>\]<track#\> - <track title><ext>

    Note that both <disc#\> and <track#\> are formatted to "%02d".

    .PARAMETER Root

    Root directory to scan for music files.

    .PARAMETER Extensions

    Specify extension list to be scanned.

    .PARAMETER IgnoreExtensions

    Specify extension list to be ignored.

    .INPUTS

    None.

    .OUTPUTS

    None.

    .EXAMPLE
    
    Rename-MusicFiles -Root "$env:USERPROFILE/Music"

    .EXAMPLE
    
    Rename-MusicFiles -Extensions '.mp3', '.flac', '.wav' -Root "$env:USERPROFILE/Music"

    .NOTES

    Author: Levente Liu
    Version: 1.0

    #>

    [CmdletBinding()]Param(
        [String]$Root = "${env:USERPROFILE}/Music",
        [String[]]$Extensions = @('.flac', '.mp3', '.wav', '.wma'),
        [String[]]$IgnoreExtensions = @('.jpg', '.m3u', '.sh')
    )

    # Get all directories
    $Root = (Resolve-Path -Path $Root).Path
    $alldirs = @(Get-Item -LiteralPath $Root) + (Get-ChildItem -LiteralPath $Root -Recurse -Directory)
    $fileDict = New-Object 'System.Collections.Generic.Dictionary[string,System.Collections.Generic.List[PSObject]]'

    # Explore files
    $shellObject = New-Object -COMObject Shell.Application
    foreach ($item in $alldirs) {
        $shellView = $shellObject.Namespace($item.FullName)

        Get-ChildItem -LiteralPath $item.FullName -File | ForEach-Object {
            # Check file extension
            $shellFile = $shellView.ParseName($_.Name)
            $attrExt = $shellView.GetDetailsOf($shellFile, 164)

            if (-Not ($Extensions -Contains $attrExt.ToLower())) {
                if (-Not ($IgnoreExtensions -contains $attrExt.ToLower())) {
                    Write-Warning -Message "Skip a file with unknown extension: $($_.FullName)"
                }
                return
            }

            # Elect artist
            $attrArtistOfAlbum = $shellView.GetDetailsOf($shellFile, 237)
            if ([string]::IsNullOrEmpty($attrArtistOfAlbum) -or ![string]::Compare($attrArtistOfAlbum, "(多个数值)")) {
                $attrArtistList = $shellView.GetDetailsOf($shellFile, 13)
                $attrArtistOfAlbum = ($attrArtistList -Split "; ")[0]
                Write-Verbose -Message @"
Missing attribute #232 (Artist of Album): $($_.FullName)"
    use "$attrArtistOfAlbum" in attribute #13 (Artist List) "$attrArtistList" instead
"@
            }

            $attrAlbum = $shellView.GetDetailsOf($shellFile, 14)
            $uniformArtist = $(Get-UniformArtist -Artist $attrArtistOfAlbum -Album $attrAlbum)
            if ([string]::IsNullOrEmpty($uniformArtist)) {
                Write-Warning -Message "Skip a file with unknown artist: $($_.FullName)"
                return
            }
            $newFileDir = Join-Path $Root $uniformArtist

            # Combine album
            $attrAlbum = Get-EscapedFilePath($attrAlbum)
            if ([string]::IsNullOrEmpty($attrAlbum)) {
                Write-Warning -Message "Album not found, skip album level directory: $($_.FullName)"
            }
            else {
                $newFileDir = Join-Path $newFileDir $attrAlbum

                # Parse disc set info. Could be like "1" or "1/3"
                $attrPart = $shellView.GetDetailsOf($shellFile, 249)
                $discIndex, $discTotal = $attrPart.Split('/')[0, 1]
                $discIndexNum = [int]$discIndex
                $discTotalNum = [int]$discTotal

                $discDir = [string]$null
                if ($discIndexNum -gt 0) {
                    $discDir = "disc {0:D2}" -f $discIndexNum
                }

                if ($discIndexNum -gt 1 -Or $discTotalNum -gt 1) {
                    $newFileDir = Join-Path $newFileDir $discDir
                }
                elseif (Test-Path -LiteralPath $newFileDir -PathType Container) {
                    $countTemp = 0
                    Get-ChildItem -LiteralPath $newFileDir -Directory | ForEach-Object {
                        if ($_.Name -match '^disc \d\d$' -And $_.Name -ne 'disc 01') { $countTemp++ }
                    }
                    if ($countTemp -gt 0) {
                        if ($discIndexNum -le 0) {
                            Write-Warning -Message "Disc index not found, use default index 01: $($_.FullName)"
                            $discDir = "disc 01"
                        }
                        $newFileDir = Join-Path $newFileDir $discDir
                    }
                }
            }

            # Combine new file name
            $attrNumber = $shellView.GetDetailsOf($shellFile, 26)
            $trackNumber = [int]$attrNumber
            if ($trackNumber -le 0 -And -Not [string]::IsNullOrEmpty($attrAlbum)) {
                Write-Warning -Message "Track number not found, use default track number 01: $($_.FullName)"
                $trackNumber = 1
            }

            $attrTitle = $shellView.GetDetailsOf($shellFile, 21).Trim()
            if ([string]::IsNullOrEmpty($attrTitle)) {
                Write-Warning -Message "Skip a file with unknown title: $($_.FullName)"
                return
            }

            $newFileName = Get-EscapedFilePath("{0}{1}" -f $attrTitle, $attrExt)
            if (-Not [string]::IsNullOrEmpty($attrAlbum)) {
                $newFileName = Get-EscapedFilePath("{0:D2} - {1}" -f $trackNumber, $newFileName)
            }
            $newFileNamePart = $newFileName -replace "$attrExt$"
            $newFilePath = Join-Path $newFileDir $newFileName
            $newFilePathName = Join-Path $newFileDir $newFileNamePart

            if ($null -eq $fileDict[$newFilePathName]) {
                $fileDict[$newFilePathName] = New-Object 'System.Collections.Generic.List[PSObject]'
            }

            # Get other attributes
            $attrSize = $shellView.GetDetailsOf($shellFile, 1)
            $attrBitRate = $shellView.GetDetailsOf($shellFile, 28)

            $fileDict[$newFilePathName].Add($(New-Object psobject -Property @{
                        "origin"  = $_.FullName
                        "target"  = $newFilePath
                        "newdir"  = $newFileDir
                        "size"    = $attrSize
                        "bitrate" = $attrBitRate
                        "ext"     = $attrExt
                    }))
        }
    }

    # Analyze plans
    $plans = New-Object 'System.Collections.Generic.List[PSObject]'
    $planIndex = 0
    foreach ($key in $fileDict.Keys) {
        # Select the best version
        $files = $fileDict[$key]
        $bestIndex = 0
        for ($i = 1; $i -lt $files.Count; $i ++) {
            if ($(Compare-MusicFileObject $files[$i] $files[$bestIndex])) {
                $bestIndex = $i
            }
        }
        $bestFile = $files[$bestIndex]
        $files.RemoveAt($bestIndex)

        # Rename or remove files
        if ($bestFile.'origin' -ne $bestFile.'target' -or $files.Count -gt 0) {
            $planIndex++

            # Detail logs for plan
            Write-Host -ForegroundColor Yellow "PLAN #$planIndex $key"
            Write-Host -ForegroundColor Green $(" ✓ | {0,8} | {1,8} | {2}" -f $bestFile.'size', $bestFile.'bitrate', $bestFile.'origin')
            if ($bestFile.'origin' -ne $bestFile.'target') {
                Write-Host -ForegroundColor Cyan "   +----------+---------> $($bestFile.'target')"
            }
            foreach ($rmFile in $files) {
                Write-Host -ForegroundColor Red $(" ✗ | {0,8} | {1,8} | {2}" -f $rmFile.'size', $rmFile.'bitrate', $rmFile.'origin')
            }

            $plans.Add($(New-Object psobject -Property @{
                        "key"    = $key
                        "keep"   = $bestFile
                        "remove" = $files
                    }))
        }
    }

    # Execute plans
    if ($plans.Count -eq 0) {
        Write-Host -ForegroundColor Green "Great, everything is in its place!"
        return
    }

    $resp = Get-Continue
    if ($resp -eq 'n') {
        break
    }

    $planIndex = 0
    foreach ($plan in $plans) {
        $planIndex++
        $keep = $plan.'keep'
        $remove = $plan.'remove'
        Write-Host -ForegroundColor Yellow "EXEC #$planIndex $($plan.'key')"
        if ($keep.'origin' -ne $keep.'target') {
            New-Item -Force -ItemType Directory -Path $keep.'newdir'
            Move-Item -LiteralPath $keep.'origin' -Destination $keep.'target' -Force
        }
        foreach ($rmFile in $remove) {
            Remove-Item -LiteralPath $rmFile.'origin'
        }
    }
}

Function Test-MusicFiles {
    <#
    .SYNOPSIS

    Test music directory tree.

    .DESCRIPTION

    Test a music directory tree, report any confilct and remove empty directories.

    .PARAMETER Root

    Root directory to scan for music files.

    .PARAMETER RemoveEmptyDir

    Remove any empty directory.

    .INPUTS

    None.

    .OUTPUTS

    None.

    .EXAMPLE
    
    Test-MusicFiles -Root "$env:USERPROFILE/Music"

    .EXAMPLE
    
    Test-MusicFiles -Root "$env:USERPROFILE/Music" -RemoveEmptyDir

    .NOTES

    Author: Levente Liu
    Version: 1.0

    #>

    [CmdletBinding()]Param(
        [string]$Root = $PWD,
        [switch]$RemoveEmptyDir
    )

    Get-ChildItem -LiteralPath $Root -Directory | ForEach-Object {
        Get-ChildItem -LiteralPath $_.FullName -Directory | ForEach-Object {
            Get-ChildItem -LiteralPath $_.FullName -Directory | ForEach-Object {
                $fileCount = (Get-ChildItem -LiteralPath $_.FullName -File | Measure-Object).Count
                if ($fileCount -eq 0) {
                    Write-Warning -Message "--- Empty disc directory: $($_.FullName)"
                    if ($RemoveEmptyDir) {
                        Remove-Item -Force -LiteralPath $_.FullName
                    }
                }
                else {
                    Write-Verbose -Message ">>> Disc directory status OK: $($_.FullName)"
                }
            }

            $dirCount = (Get-ChildItem -LiteralPath $_.FullName -Directory | Measure-Object).Count
            $fileCount = (Get-ChildItem -LiteralPath $_.FullName -File | Measure-Object).Count
            if ($dirCount -eq 0 -And $fileCount -eq 0) {
                Write-Warning -Message "-- Empty album directory: $($_.FullName)"
                if ($RemoveEmptyDir) {
                    Remove-Item -Force -LiteralPath $_.FullName
                }
            }
            elseif ($dirCount -gt 0 -And $fileCount -gt 0) {
                Write-Warning -Message @"
!! Conflicting album directory: $($_.FullName)
    NEED MANUAL PROCESS
"@
            }
            else {
                Write-Verbose -Message ">> Album directory status OK: $($_.FullName)"
            }
        }

        $dirCount = (Get-ChildItem -LiteralPath $_.FullName -Directory | Measure-Object).Count
        $fileCount = (Get-ChildItem -LiteralPath $_.FullName -File | Measure-Object).Count
        if ($dirCount -eq 0 -And $fileCount -eq 0) {
            Write-Warning -Message "- Empty artist directory: $($_.FullName)"
            if ($RemoveEmptyDir) {
                Remove-Item -Force -LiteralPath $_.FullName
            }
        }
        elseif ($fileCount -gt 0) {
            Write-Warning -Message @"
! Conflicting artist directory: $($_.FullName)
    NEED MANUAL PROCESS
"@
        }
        else {
            Write-Verbose -Message "> Artist directory status OK: $($_.FullName)"
        }
    }
}

Function Show-FileAttrList {
    <#
    .SYNOPSIS

    Show file attribute list.

    .DESCRIPTION

    Show each file attribute with format: <attr#\>: <description>.

    .INPUTS

    None.

    .OUTPUTS

    File attribute list.

    .NOTES

    Author: Levente Liu
    Version: 1.0

    #>
    $shellObject = New-Object -COMObject Shell.Application
    $shellView = $shellObject.NameSpace("C:\")
    for ($i = 0; $i -le 315; $i++) {
        '{0:D3}# {1}' -f $i, $shellView.GetDetailsOf(0, $i)
    }
}

Export-ModuleMember -Function Rename-MusicFiles, Show-FileAttrList, Test-MusicFiles
