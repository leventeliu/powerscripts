# README

These functions only use the attributes within your local files to reorganize them. Files lacking some essential attributes won't be able to be categorized correctly.

## Set Execution Policy

_Warning: these scripts are not signed. Changing your execution policy means you trust them._

RemoteSigned should be enough to run the scripts.

```powershell
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned
```

## Install Modules

```powershell
.\Install.ps1
```

## Scan-Only

Scan all music files under `<root>` and print results.

```powershell
Rename-MusicFiles -Root <root>
```

Add `-Verbose` switch for more detailed output.

## Check and Move

If it looks good to you, add `-CheckAndMove` switch and run scan again to actually move files.

```powershell
Rename-MusicFiles -Root <root> -CheckAndMove
```

## Check Conflict

Use `Test-MusicFiles` to check any conflict.

```powershell
Test-MusicFiles -Root <root>
```

Add `-RemoveEmptyDir` to remove empty directory automatically.

Add `-Verbose` switch for more detailed output.
